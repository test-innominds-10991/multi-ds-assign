package com.inno;

import java.io.Serializable;

public class SerializeDemo implements Serializable
{
	String a;
	int b;
	transient int c;
	public SerializeDemo(String a,int b,int c)
	{
		this.a=a;
		this.b=b;
		this.c=c;
	}

}
