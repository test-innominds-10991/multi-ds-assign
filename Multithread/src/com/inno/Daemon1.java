package com.inno;

public class Daemon1 extends Thread
{
	public void run()
	{
		if(Thread.currentThread().isDaemon())
		{
			System.out.println("daemon is working");
		}
		else
		{
			System.out.println("User thread works");
		}
	}
	public static void main(String[] args)
	{
		Daemon1 d1 = new Daemon1();
		Daemon1 d2 = new Daemon1();
		Daemon1 d3 = new Daemon1();
		
		d1.setDaemon(true);
		d1.start();
		d2.start();
		d3.start();
	}

}
