package com.inno;

public class PriorThread extends Thread
{
	public void run()
	{
		System.out.println("Inside the method");
	}
	public static void main(String[] args) throws InterruptedException
	{
		PriorThread t1 = new PriorThread();
		PriorThread t2 = new PriorThread();
		PriorThread t3 = new PriorThread();
		PriorThread t4 = new PriorThread();
		PriorThread t5 = new PriorThread();
		
		System.out.println("t1 thread priority:" + t1.getPriority());
		System.out.println("t2 thread priority:" + t2.getPriority());
		System.out.println("t3 thread priority:" + t3.getPriority());
		System.out.println("t4 thread priority:" + t4.getPriority());
		System.out.println("t5 thread priority;" + t5.getPriority());
		
		t1.setPriority(2);
		t2.setPriority(3);
		t3.setPriority(5);
		t4.setPriority(7);
		t5.setPriority(9);
		
		t1.start();
		
		if(t1.isAlive())
			System.out.println("Thread 1 is alive");
		else
			System.out.println("Thread 1 is not alive");
		t2.start();
		if(t2.isAlive())
			System.out.println("Thread 2 is alive");
		t3.start();
		if(t3.isAlive())
			System.out.println("Thread 3 is not alive");
		else
			System.out.println("Thread 3 is not alive");
		t4.sleep(1000);
		if(t4.isAlive())
			System.out.println("Thread 4 is alive");
		else
			System.out.println("Thread 4 is not alive");
		t5.sleep(1500);
		if(t5.isAlive())
			System.out.println("Thread 5 is alive");
		else
			System.out.println("Thread 5 is not alive");		
	}
}
