package com.inno;
class ThreadRunnable implements Runnable
{
	Thread t;
	ThreadRunnable(String name)
	{
		t = new Thread(this , name);
		t.start();
	}
	public void run()
	{
		for(int i=1;i<=5;i++)
		{
			try
			{
				if(t.getName().equals("First thread"));
				{
					Thread.sleep(1000);
					System.out.println(t.getName()+": Good Morning");
				}
				else if(t.getName().equals("Second Thread"))
				{
					Thread.sleep(2000);
					System.out.println(t.getName()+" : Hello");
				}
				else
				{
					Thread.sleep(3000);
					System.out.println(t.getName()+": Welcome");
				}
			}
			catch(InterruptedException e)
			{
				System.out.println(t.getName()+ " is interrupted");
			}
		}
	}
	class FirstRunnable
	{
		public static void main(String[] args)
		{
			ThreadRunnable t1 = new ThreadRunnable("first thread");
			ThreadRunnable t2 = new ThreadRunnable("Second thread");
			ThreadRunnable t3 = new ThreadRunnable("Third thread");
		}
	}
}