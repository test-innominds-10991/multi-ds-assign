package com.inno;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Transient 
{
	public static void main(String[] args) 
	{
		try
		{
			SerializeDemo s=new SerializeDemo("Reddy",10,20);
			FileOutputStream f=new FileOutputStream("C:\\Users\\sparupati\\Desktop\\Snehith.txt");
			ObjectOutputStream o=new ObjectOutputStream(f);
			o.writeObject(s);
			o.flush();
			f.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try
		{
			FileInputStream f=new FileInputStream("C:\\Users\\sparupati\\Desktop\\Snehith.txt");
			ObjectInputStream o=new ObjectInputStream(f);
			SerializeDemo s=(SerializeDemo)o.readObject();
			System.out.println(s.a+" "+s.b+" "+s.c);
			o.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
