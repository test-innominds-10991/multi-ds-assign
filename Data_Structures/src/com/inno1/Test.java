package com.inno1;

public class Test 
{
	public static void main(String[] args)
	{
		EmployeeDB empDB = new EmployeeDB();
		Employee e1 = new Employee(011,"Anudeep","anu@gmail.com",'m',25000);
		Employee e2 = new Employee(022,"Sumith","sumith@gmail.com",'m',28000);
		Employee e3 = new Employee(033,"Snehith","snehith@gmail.com",'m',31000);
		
		empDB.addEmployee(e1);
		empDB.addEmployee(e2);
		empDB.addEmployee(e3);
		
		for(Employee emp : empDB.listAll())
			System.out.println(emp.GetEmployeeDetails());
		System.out.println();
		empDB.deleteEmployee(022);
		
		for(Employee emp : empDB.listAll())
			System.out.println(emp.GetEmployeeDetails());
		
		System.out.println();
		System.out.println(empDB.showPaySlip(3));
	}

}
